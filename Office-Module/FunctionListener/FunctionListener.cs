﻿using Office_Module.ConverterHelper;
using Office_Module.Notifications;
using Ontology_Module;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Office_Module.FunctionListener
{
    public partial class FunctionListener : Form
    {
        private FunctionListenerViewModelController viewModelController;

        private clsLocalConfig localConfig;

        private frmMain frmMain;

        public FunctionListener()
        {
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            InitializeComponent();

            

            Initialize();
        }

       

        private void Initialize()
        {
            
            viewModelController = new FunctionListenerViewModelController(localConfig);
            viewModelController.PropertyChanged += ViewModelController_PropertyChanged;
            viewModelController.closeFunctionListner += ViewModelController_closeFunctionListner;
            viewModelController.addOItem += ViewModelController_addOItem;

            ViewModelController_PropertyChanged(this, new PropertyChangedEventArgs(null));

            
        }

        private void ViewModelController_addOItem()
        {
            if (this.InvokeRequired)
            {
                var deleg = new AddOItem(ViewModelController_addOItem);
                this.Invoke(deleg);
            }
            else
            {
                List<clsOntologyItem> selectedItems = null;
                frmMain = new frmMain(localConfig.Globals);
                frmMain.ShowDialog(this);
                if (frmMain.DialogResult == DialogResult.OK)
                {
                    selectedItems = frmMain.OList_Simple;
                }

                viewModelController.SelectedOItems(selectedItems);
            }
        }

        private void ViewModelController_closeFunctionListner()
        {
            Close();
        }

        private void ViewModelController_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (this.InvokeRequired)
            {
                var deleg = new PropertyChangedEventHandler(ViewModelController_PropertyChanged);
                this.Invoke(deleg, sender, e);
            }
            else
            {
                if (e.PropertyName == NotifyItems.FunctionListener_Text_Close || e.PropertyName == null)
                {
                    toolStripButton_Close.Text = viewModelController.Text_Close;
                }
                if (e.PropertyName == NotifyItems.FunctionListener_DropDownItems || e.PropertyName == null)
                {
                    toolStripDropDownButton_Function.DropDownItems.Clear();
                    viewModelController.DropDownItems.OrderBy(dropDownItem => dropDownItem.OrderId).ThenBy(dropDownItem => dropDownItem.DisplayName).ToList().ForEach(dropDownItem =>
                    {
                        dropDownItem.AssignDropDownButton(toolStripDropDownButton_Function);
                    });
                }
                if (e.PropertyName == NotifyItems.FunctionListener_SelectedDropDownItem || e.PropertyName == null)
                {
                    toolStripDropDownButton_Function.Text = viewModelController.SelectedDropDownItem.DisplayName;
                }
                if (e.PropertyName == NotifyItems.FunctionListener_AppliedItems || e.PropertyName == null)
                {
                    dataGridView_AppliedItems.DataSource = new SortableBindingList<clsOntologyItem>(viewModelController.AppliedItems);
                    dataGridView_AppliedItems.Columns.Cast<DataGridViewColumn>().ToList().ForEach(col =>
                    {
                        col.Visible = col.DataPropertyName == "Name_Item";
                    });
                }
                if (e.PropertyName == NotifyItems.FunctionListener_IsChecked_Activate || e.PropertyName == null)
                {
                    toolStripButton_Activate.Checked = viewModelController.IsChecked_Activate;
                }
                if (e.PropertyName == NotifyItems.FunctionListener_Text_Activate || e.PropertyName == null)
                {
                    toolStripButton_Activate.Text = viewModelController.Text_Activate;
                }
                if (e.PropertyName == NotifyItems.FunctionListener_Text_Close || e.PropertyName == null)
                {
                    toolStripButton_Close.Text = viewModelController.Text_Close;
                }
                if (e.PropertyName == NotifyItems.FunctionListener_Image_ActivateBookmark || e.PropertyName == null)
                {
                    toolStripButton_ActivateBookmark.Image = viewModelController.Image_ActivateBookmark;
                }
                if (e.PropertyName == NotifyItems.FunctionListener_Image_OpenDoc || e.PropertyName == null)
                {
                    toolStripButton_OpenDoc.Image = viewModelController.Image_OpenDoc;
                }
                if (e.PropertyName == NotifyItems.FunctionListener_Image_InsertBookmark || e.PropertyName == null)
                {
                    toolStripButton_InsertBookmark.Image = viewModelController.Image_InsertBookmark;
                }
                if (e.PropertyName == NotifyItems.FunctionListener_Image_InsertItems || e.PropertyName == null)
                {
                    toolStripButton_InsertItem.Image = viewModelController.Image_InsertItems;
                }
                if (e.PropertyName == NotifyItems.FunctionListener_IsEnabled_ActivateBookmark || e.PropertyName == null)
                {
                    toolStripButton_ActivateBookmark.Enabled = viewModelController.IsEnabled_ActivateBookmark;
                }
                if (e.PropertyName == NotifyItems.FunctionListener_IsEnabled_InsertBookmark || e.PropertyName == null)
                {
                    toolStripButton_InsertBookmark.Enabled = viewModelController.IsEnabled_InsertBookmark;
                }
                if (e.PropertyName == NotifyItems.FunctionListener_IsEnabled_InsertItems || e.PropertyName == null)
                {
                    toolStripButton_InsertItem.Enabled = viewModelController.IsEnabled_InsertItems;
                }
                if (e.PropertyName == NotifyItems.FunctionListener_IsEnabled_OpenDoc || e.PropertyName == null)
                {
                    toolStripButton_OpenDoc.Enabled = viewModelController.IsEnabled_OpenDoc;
                }
                if (e.PropertyName == NotifyItems.FunctionListener_IsVisible_ActivateBookmark || e.PropertyName == null)
                {
                    toolStripButton_ActivateBookmark.Visible = viewModelController.IsVisible_ActivateBookmark;
                }
                if (e.PropertyName == NotifyItems.FunctionListener_IsVisible_ActivateBookmarkText || e.PropertyName == null)
                {
                    toolStripButton_ActivateBookmark.DisplayStyle = TextVisibilityToDisplayStyleConverter.Convert(viewModelController.IsVisible_ActivateBookmarkText);
                }
                if (e.PropertyName == NotifyItems.FunctionListener_IsVisible_InsertBookmarkText || e.PropertyName == null)
                {
                    toolStripButton_InsertBookmark.DisplayStyle = TextVisibilityToDisplayStyleConverter.Convert(viewModelController.IsVisible_InsertBookmarkText);
                }
                if (e.PropertyName == NotifyItems.FunctionListener_IsVisible_InsertItemsText || e.PropertyName == null)
                {
                    toolStripButton_InsertItem.DisplayStyle = TextVisibilityToDisplayStyleConverter.Convert(viewModelController.IsVisible_InsertItemsText);
                }
                if (e.PropertyName == NotifyItems.FunctionListener_IsVisible_OpenDocText || e.PropertyName == null)
                {
                    toolStripButton_OpenDoc.DisplayStyle = TextVisibilityToDisplayStyleConverter.Convert(viewModelController.IsVisible_OpenDocText);
                }
                if (e.PropertyName == NotifyItems.FunctionListener_FormMessage)
                {
                    MessageBox.Show(this, viewModelController.FormMessage.Message, viewModelController.FormMessage.Short, FormMessageToButtonConverter.Convert(viewModelController.FormMessage), FormMessageToIconConverter.Convert(viewModelController.FormMessage));
                }
                if (e.PropertyName == NotifyItems.FunctionListener_Text_AddItem || e.PropertyName == null)
                {
                    toolStripButton_AddOitem.Text = viewModelController.Text_AddItem;
                }
                if (e.PropertyName == NotifyItems.FunctionListener_IsEnabled_AddItem || e.PropertyName == null)
                {
                    toolStripButton_AddOitem.Enabled = viewModelController.IsEnabled_AddItem;
                }
                if (e.PropertyName == NotifyItems.FunctionListener_IsVisible_AddItem || e.PropertyName == null)
                {
                    toolStripButton_AddOitem.Visible = viewModelController.IsVisible_AddItem;
                }
                if (e.PropertyName == NotifyItems.FunctionListener_Text_SelectedButton || e.PropertyName == null)
                {
                    toolStripLabel_SelectedButton.Text = viewModelController.Text_SelectedButton;
                }
                if (e.PropertyName == NotifyItems.FunctionListener_Text_ActivateBookmark || e.PropertyName == null)
                {
                    toolStripButton_ActivateBookmark.Text = viewModelController.Text_ActivateBookmark;
                }
                if (e.PropertyName == NotifyItems.FunctionListener_Text_InsertBookmark || e.PropertyName == null)
                {
                    toolStripButton_InsertBookmark.Text = viewModelController.Text_InsertBookmark;
                }
                if (e.PropertyName == NotifyItems.FunctionListener_Text_InsertItems || e.PropertyName == null)
                {
                    toolStripButton_InsertItem.Text = viewModelController.Text_InsertItems;
                }
                if (e.PropertyName == NotifyItems.FunctionListener_Text_OpenDoc || e.PropertyName == null)
                {
                    toolStripButton_OpenDoc.Text = viewModelController.Text_OpenDoc;
                }
                if (e.PropertyName == NotifyItems.FunctionListener_Text_Form || e.PropertyName == null)
                {
                    this.Text = viewModelController.Text_Form;
                }

            }
        }

        private void toolStripButton_Close_Click(object sender, EventArgs e)
        {
            viewModelController.CloseFunctionListener();
        }

        private void toolStripButton_Activate_CheckStateChanged(object sender, EventArgs e)
        {
            viewModelController.IsChecked_Activate = toolStripButton_Activate.Checked;
        }

        private void FunctionListener_FormClosing(object sender, FormClosingEventArgs e)
        {
            viewModelController.CloseForm();
        }

        private void dataGridView_AppliedItems_SelectionChanged(object sender, EventArgs e)
        {
            viewModelController.SelectedItems = dataGridView_AppliedItems.SelectedRows.Cast<DataGridViewRow>().Select(rowItem => (clsOntologyItem) rowItem.DataBoundItem).ToList();

        }

        private void toolStripButton_OpenDoc_Click(object sender, EventArgs e)
        {
            viewModelController.OpenDoc();
        }

        private void toolStripButton_AddOitem_Click(object sender, EventArgs e)
        {
            viewModelController.AddOItems();
        }

        private void toolStripButton_OpenDoc_MouseHover(object sender, EventArgs e)
        {
            
        }

        private void toolStripButton_OpenDoc_MouseEnter(object sender, EventArgs e)
        {
            viewModelController.Text_SelectedButton = toolStripButton_OpenDoc.Text;
        }

        private void toolStripButton_OpenDoc_MouseLeave(object sender, EventArgs e)
        {
            viewModelController.Text_SelectedButton = "";
        }

        private void toolStripButton_InsertBookmark_MouseEnter(object sender, EventArgs e)
        {
            viewModelController.Text_SelectedButton = toolStripButton_InsertBookmark.Text;
        }

        private void toolStripButton_InsertBookmark_MouseLeave(object sender, EventArgs e)
        {
            viewModelController.Text_SelectedButton = "";
        }

        private void toolStripButton_ActivateBookmark_MouseEnter(object sender, EventArgs e)
        {
            viewModelController.Text_SelectedButton = toolStripButton_ActivateBookmark.Text;
        }

        private void toolStripButton_ActivateBookmark_MouseLeave(object sender, EventArgs e)
        {
            viewModelController.Text_SelectedButton = "";
        }

        private void toolStripButton_InsertItem_MouseEnter(object sender, EventArgs e)
        {
            viewModelController.Text_SelectedButton = toolStripButton_InsertItem.Text;
        }

        private void toolStripButton_InsertItem_MouseLeave(object sender, EventArgs e)
        {
            viewModelController.Text_SelectedButton = "";
        }
    }
}
