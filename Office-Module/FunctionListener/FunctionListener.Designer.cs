﻿namespace Office_Module.FunctionListener
{
    partial class FunctionListener
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FunctionListener));
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton_Close = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel_SelectedButton = new System.Windows.Forms.ToolStripLabel();
            this.dataGridView_AppliedItems = new System.Windows.Forms.DataGridView();
            this.toolStrip3 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton_OpenDoc = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_InsertBookmark = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_ActivateBookmark = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_InsertItem = new System.Windows.Forms.ToolStripButton();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolStripDropDownButton_Function = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_Activate = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_AddOitem = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer1.BottomToolStripPanel.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.RightToolStripPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_AppliedItems)).BeginInit();
            this.toolStrip3.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.BottomToolStripPanel
            // 
            this.toolStripContainer1.BottomToolStripPanel.Controls.Add(this.toolStrip1);
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.dataGridView_AppliedItems);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(543, 252);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            // 
            // toolStripContainer1.RightToolStripPanel
            // 
            this.toolStripContainer1.RightToolStripPanel.Controls.Add(this.toolStrip3);
            this.toolStripContainer1.Size = new System.Drawing.Size(575, 302);
            this.toolStripContainer1.TabIndex = 0;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip2);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton_Close,
            this.toolStripSeparator3,
            this.toolStripLabel_SelectedButton});
            this.toolStrip1.Location = new System.Drawing.Point(3, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(58, 25);
            this.toolStrip1.TabIndex = 0;
            // 
            // toolStripButton_Close
            // 
            this.toolStripButton_Close.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton_Close.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Close.Image")));
            this.toolStripButton_Close.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Close.Name = "toolStripButton_Close";
            this.toolStripButton_Close.Size = new System.Drawing.Size(40, 22);
            this.toolStripButton_Close.Text = "Close";
            this.toolStripButton_Close.Click += new System.EventHandler(this.toolStripButton_Close_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel_SelectedButton
            // 
            this.toolStripLabel_SelectedButton.Font = new System.Drawing.Font("Segoe UI", 7F);
            this.toolStripLabel_SelectedButton.Name = "toolStripLabel_SelectedButton";
            this.toolStripLabel_SelectedButton.Size = new System.Drawing.Size(0, 22);
            // 
            // dataGridView_AppliedItems
            // 
            this.dataGridView_AppliedItems.AllowUserToAddRows = false;
            this.dataGridView_AppliedItems.AllowUserToDeleteRows = false;
            this.dataGridView_AppliedItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_AppliedItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_AppliedItems.Location = new System.Drawing.Point(0, 0);
            this.dataGridView_AppliedItems.Name = "dataGridView_AppliedItems";
            this.dataGridView_AppliedItems.ReadOnly = true;
            this.dataGridView_AppliedItems.Size = new System.Drawing.Size(543, 252);
            this.dataGridView_AppliedItems.TabIndex = 0;
            this.dataGridView_AppliedItems.SelectionChanged += new System.EventHandler(this.dataGridView_AppliedItems_SelectionChanged);
            // 
            // toolStrip3
            // 
            this.toolStrip3.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton_OpenDoc,
            this.toolStripButton_InsertBookmark,
            this.toolStripButton_ActivateBookmark,
            this.toolStripButton_InsertItem});
            this.toolStrip3.Location = new System.Drawing.Point(0, 3);
            this.toolStrip3.Name = "toolStrip3";
            this.toolStrip3.ShowItemToolTips = false;
            this.toolStrip3.Size = new System.Drawing.Size(32, 122);
            this.toolStrip3.TabIndex = 0;
            // 
            // toolStripButton_OpenDoc
            // 
            this.toolStripButton_OpenDoc.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_OpenDoc.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_OpenDoc.Image")));
            this.toolStripButton_OpenDoc.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_OpenDoc.Name = "toolStripButton_OpenDoc";
            this.toolStripButton_OpenDoc.Size = new System.Drawing.Size(30, 20);
            this.toolStripButton_OpenDoc.Text = "toolStripButton1";
            this.toolStripButton_OpenDoc.Click += new System.EventHandler(this.toolStripButton_OpenDoc_Click);
            this.toolStripButton_OpenDoc.MouseEnter += new System.EventHandler(this.toolStripButton_OpenDoc_MouseEnter);
            this.toolStripButton_OpenDoc.MouseLeave += new System.EventHandler(this.toolStripButton_OpenDoc_MouseLeave);
            this.toolStripButton_OpenDoc.MouseHover += new System.EventHandler(this.toolStripButton_OpenDoc_MouseHover);
            // 
            // toolStripButton_InsertBookmark
            // 
            this.toolStripButton_InsertBookmark.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_InsertBookmark.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_InsertBookmark.Image")));
            this.toolStripButton_InsertBookmark.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_InsertBookmark.Name = "toolStripButton_InsertBookmark";
            this.toolStripButton_InsertBookmark.Size = new System.Drawing.Size(30, 20);
            this.toolStripButton_InsertBookmark.Text = "toolStripButton1";
            this.toolStripButton_InsertBookmark.MouseEnter += new System.EventHandler(this.toolStripButton_InsertBookmark_MouseEnter);
            this.toolStripButton_InsertBookmark.MouseLeave += new System.EventHandler(this.toolStripButton_InsertBookmark_MouseLeave);
            // 
            // toolStripButton_ActivateBookmark
            // 
            this.toolStripButton_ActivateBookmark.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_ActivateBookmark.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_ActivateBookmark.Image")));
            this.toolStripButton_ActivateBookmark.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_ActivateBookmark.Name = "toolStripButton_ActivateBookmark";
            this.toolStripButton_ActivateBookmark.Size = new System.Drawing.Size(30, 20);
            this.toolStripButton_ActivateBookmark.Text = "toolStripButton2";
            this.toolStripButton_ActivateBookmark.MouseEnter += new System.EventHandler(this.toolStripButton_ActivateBookmark_MouseEnter);
            this.toolStripButton_ActivateBookmark.MouseLeave += new System.EventHandler(this.toolStripButton_ActivateBookmark_MouseLeave);
            // 
            // toolStripButton_InsertItem
            // 
            this.toolStripButton_InsertItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_InsertItem.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_InsertItem.Image")));
            this.toolStripButton_InsertItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_InsertItem.Name = "toolStripButton_InsertItem";
            this.toolStripButton_InsertItem.Size = new System.Drawing.Size(30, 20);
            this.toolStripButton_InsertItem.Text = "toolStripButton3";
            this.toolStripButton_InsertItem.MouseEnter += new System.EventHandler(this.toolStripButton_InsertItem_MouseEnter);
            this.toolStripButton_InsertItem.MouseLeave += new System.EventHandler(this.toolStripButton_InsertItem_MouseLeave);
            // 
            // toolStrip2
            // 
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton_Function,
            this.toolStripSeparator1,
            this.toolStripButton_Activate,
            this.toolStripSeparator2,
            this.toolStripButton_AddOitem});
            this.toolStrip2.Location = new System.Drawing.Point(3, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(311, 25);
            this.toolStrip2.TabIndex = 0;
            // 
            // toolStripDropDownButton_Function
            // 
            this.toolStripDropDownButton_Function.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton_Function.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton_Function.Image")));
            this.toolStripDropDownButton_Function.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton_Function.Name = "toolStripDropDownButton_Function";
            this.toolStripDropDownButton_Function.Size = new System.Drawing.Size(164, 22);
            this.toolStripDropDownButton_Function.Text = "toolStripDropDownButton1";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton_Activate
            // 
            this.toolStripButton_Activate.CheckOnClick = true;
            this.toolStripButton_Activate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton_Activate.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Activate.Image")));
            this.toolStripButton_Activate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Activate.Name = "toolStripButton_Activate";
            this.toolStripButton_Activate.Size = new System.Drawing.Size(54, 22);
            this.toolStripButton_Activate.Text = "Activate";
            this.toolStripButton_Activate.CheckStateChanged += new System.EventHandler(this.toolStripButton_Activate_CheckStateChanged);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton_AddOitem
            // 
            this.toolStripButton_AddOitem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton_AddOitem.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_AddOitem.Image")));
            this.toolStripButton_AddOitem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_AddOitem.Name = "toolStripButton_AddOitem";
            this.toolStripButton_AddOitem.Size = new System.Drawing.Size(69, 22);
            this.toolStripButton_AddOitem.Text = "Add Item...";
            this.toolStripButton_AddOitem.Click += new System.EventHandler(this.toolStripButton_AddOitem_Click);
            // 
            // FunctionListener
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(575, 302);
            this.Controls.Add(this.toolStripContainer1);
            this.Name = "FunctionListener";
            this.Text = "FunctionListener";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FunctionListener_FormClosing);
            this.toolStripContainer1.BottomToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.BottomToolStripPanel.PerformLayout();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.RightToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.RightToolStripPanel.PerformLayout();
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_AppliedItems)).EndInit();
            this.toolStrip3.ResumeLayout(false);
            this.toolStrip3.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton_Close;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton_Function;
        private System.Windows.Forms.DataGridView dataGridView_AppliedItems;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButton_Activate;
        private System.Windows.Forms.ToolStrip toolStrip3;
        private System.Windows.Forms.ToolStripButton toolStripButton_OpenDoc;
        private System.Windows.Forms.ToolStripButton toolStripButton_InsertBookmark;
        private System.Windows.Forms.ToolStripButton toolStripButton_ActivateBookmark;
        private System.Windows.Forms.ToolStripButton toolStripButton_InsertItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton_AddOitem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripLabel toolStripLabel_SelectedButton;
    }
}