﻿using Office_Module.ConverterHelper;
using Office_Module.Notifications;
using Ontology_Module;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Office_Module.FunctionListener
{
    public delegate void CloseFunctionListner();
    public delegate void AddOItem();
    public class FunctionListenerViewModelController : FunctionListenerViewModel
    {
        public event CloseFunctionListner closeFunctionListner;
        public event AddOItem addOItem;

        private clsDocumentation documentation;

        private clsLocalConfig localConfig;

        private DropDownItem dropDownItem_InsertItems;
        private DropDownItem dropDownItem_InsertBookMark;

        private OntologyModDBConnector objDBLevel_ItemType;

        private Thread threadModExchangeServer;

        private clsOntologyItem objOItem_Document_Opened = null;

        public void CloseFunctionListener()
        {
            if (closeFunctionListner != null)
            {
                closeFunctionListner();
            }
        }

        public FunctionListenerViewModelController(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
            Initialize();
        }

        private void Initialize()
        {
            try
            {
                threadModExchangeServer.Abort();
                ModuleDataExchanger.Disconnect();
            }
            catch (Exception ex)
            {

            }
            threadModExchangeServer = new Thread(ModExchangeHandler);
            threadModExchangeServer.Start();
            
            DropDownItems = new List<DropDownItem>();
            dropDownItem_InsertItems = AddDropDownItem("Insert Items", true);
            dropDownItem_InsertBookMark = AddDropDownItem("Bookmark Item", false);

            Text_Activate = "Activate";
            Text_Close = "Close";
            IsChecked_Activate = false;
            Text_OpenDoc = "Open Doc";
            Text_ActivateBookmark = "Activate Bookmark";
            Text_InsertBookmark = "Insert Bookmark";
            Text_InsertItems = "Insert Items";
            Text_AddItem = "Add Item...";

            IsEnabled_ActivateBookmark = false;
            IsEnabled_InsertBookmark = false;
            IsEnabled_InsertItems = false;
            IsEnabled_OpenDoc = false;
            IsEnabled_AddItem = true;

            IsVisible_ActivateBookmark = true;
            IsVisible_InsertBookmark = true;
            IsVisible_InsertItems = true;
            IsVisible_OpenDoc = true;
            IsVisible_AddItem = true;

            Image_OpenDoc = Properties.Resources.OpenDoc;
            Image_ActivateBookmark = Properties.Resources.OpenBookmark;
            Image_InsertBookmark = Properties.Resources.InsertBookmark;
            Image_InsertItems = Properties.Resources.InsertItems;

            documentation = new clsDocumentation(localConfig);

            PropertyChanged += FunctionListenerViewModelController_PropertyChanged;

            objDBLevel_ItemType = new OntologyModDBConnector(localConfig.Globals);

            ParseArguments();
        }

        private void FunctionListenerViewModelController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == NotifyItems.FunctionListener_SelectedItems)
            {
                if (!documentation.IsWordOpen) objOItem_Document_Opened = null;

                IsEnabled_ActivateBookmark = false;
                IsEnabled_InsertBookmark = false;
                IsEnabled_InsertItems = false;
                IsEnabled_OpenDoc = false;

                if (SelectedItems.Count > 0)
                {
                    IsEnabled_InsertItems = documentation.IsWordOpen;

                    if (SelectedItems.Count == 1)
                    {
                        
                        IsEnabled_InsertBookmark = documentation.IsWordOpen;
                        IsEnabled_OpenDoc = objOItem_Document_Opened == null;

                        var objOItem_Bookmark = documentation.getBookmarksOfRef(SelectedItems.First());
                        IsEnabled_ActivateBookmark = objOItem_Bookmark != null;
                    }
                }


            }
        }

        private void ParseArguments()
        {
            var objArgumentParsing = new clsArgumentParsing(localConfig.Globals, Environment.GetCommandLineArgs().ToList());

            localConfig.objSession = new clsSession(localConfig.Globals);

            if (!string.IsNullOrEmpty(objArgumentParsing.Session))
            {
                var objOItem_Ref = new clsOntologyItem
                {
                    GUID = objArgumentParsing.Session,
                    Type = localConfig.Globals.Type_Object
                };

                localConfig.OItem_Session = objDBLevel_ItemType.GetOItem(objOItem_Ref.GUID, localConfig.Globals.Type_Object);
                objOItem_Ref = null;

                if (localConfig.OItem_Session != null)
                {
                    AppliedItems = localConfig.objSession.GetItems(localConfig.OItem_Session, true);
                    Text_Form = "Session: " + localConfig.OItem_Session.Name;
                }
                else
                {
                    objOItem_Ref = null;
                }
                
            }
            else if (objArgumentParsing.OList_Items != null && objArgumentParsing.OList_Items.Count == 1)
            {
                var objOItem_Ref = objArgumentParsing.OList_Items.First();

                AppliedItems = new List<clsOntologyItem> { objOItem_Ref };

                if (objOItem_Ref.Type == localConfig.Globals.Type_Object)
                {
                    var oItem_Class = objDBLevel_ItemType.GetOItem(objOItem_Ref.GUID_Parent, localConfig.Globals.Type_Class);
                    Text_Form = oItem_Class.Name + "\\" + objOItem_Ref.Name;
                    
                }
                else
                {
                    Text_Form = objOItem_Ref.Name;
                }
            }

        }

        private void ModExchangeHandler()
        {
            ModuleDataExchanger._serverApplyResponse += ModuleDataExchanger__serverApplyResponse;
            ModuleDataExchanger.Server(localConfig);
        }

        private void ModuleDataExchanger__serverApplyResponse(clsApplyItem[] applyItems)
        {

            AppliedItems = applyItems.Select(applyItem => new clsOntologyItem
            {
                GUID = applyItem.IdItem,
                Name = applyItem.NameItem,
                GUID_Parent = applyItem.IdItemParent,
                Type = applyItem.Type
            }).ToList();


            DoFunction();
        }

        private void DoFunction()
        {
            if (IsChecked_Activate)
            {
                if (SelectedDropDownItem == dropDownItem_InsertItems)
                {
                    documentation.InsertItemList(AppliedItems);
                }
                else if (SelectedDropDownItem == dropDownItem_InsertBookMark)
                {
                    documentation.insert_Bookmark(AppliedItems.First());
                }
            }
        }

        private DropDownItem AddDropDownItem(string displayName, bool isDefault)
        {
            var dropDownItem = new DropDownItem(displayName);
            dropDownItem.clickedDropDownButton += DropDownItem_clickedDropDownButton;
            DropDownItems.Add(dropDownItem);
            DropDownItems.ForEach(dropDownItemToSetToDefault =>
            {
                if (isDefault && dropDownItem == dropDownItemToSetToDefault)
                {
                    
                    dropDownItemToSetToDefault.IsDefault = true;
                }
                else
                {
                    dropDownItemToSetToDefault.IsDefault = false;
                }
                    
            });

            if (isDefault)
            {
                SelectedDropDownItem = dropDownItem;
            }

            return dropDownItem;
        }

        private void DropDownItem_clickedDropDownButton(DropDownItem dropDownItem)
        {
            SelectedDropDownItem = dropDownItem;
        }

        public void CloseForm()
        {
            try
            {
                threadModExchangeServer.Abort();
            }
            catch(Exception ex)
            {

            }
            

        }

        public void OpenDoc()
        {
            var selectedItem = SelectedItems.FirstOrDefault();
            objOItem_Document_Opened = documentation.open_Document(SelectedItems.First());

            if (objOItem_Document_Opened.GUID != localConfig.Globals.LState_Error.GUID)
            {
                IsEnabled_InsertBookmark = false;
                IsEnabled_OpenDoc = false;
                IsEnabled_InsertItems = false;
                IsEnabled_ActivateBookmark = false;
            }
            else
            {
                FormMessage = new FormMessage("Das Dokument konnte nicht geöffnet werden!", "Fehler", FormMessageType.Exclamation | FormMessageType.OK);
            }
        }

        public void AddOItems()
        {
            IsEnabled_AddItem = false;

            if (addOItem != null)
            {
                addOItem();
            }
        }

        public void SelectedOItems(List<clsOntologyItem> selectedItems = null)
        {
            IsEnabled_AddItem = false;

            if (selectedItems != null)
            {
                AppliedItems.AddRange(selectedItems);
                RaisePropertyChanged(NotifyItems.FunctionListener_AppliedItems);
            }
            
        }
    }
}
