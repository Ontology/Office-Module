﻿using Office_Module.BaseClasses;
using Office_Module.ConverterHelper;
using Office_Module.Notifications;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Office_Module.FunctionListener
{
    public class FunctionListenerViewModel : ViewModelBase
    { 
        private string text_Close;
        public string Text_Close
        {
            get { return text_Close; }
            set
            {
                if (text_Close == value) return;

                text_Close = value;

                RaisePropertyChanged(NotifyItems.FunctionListener_Text_Close);
            }
        }

        private List<DropDownItem> dropDownItems;
        public List<DropDownItem> DropDownItems
        {
            get { return dropDownItems; }
            set
            {
                if (dropDownItems == value) return;

                dropDownItems = value;

                RaisePropertyChanged(NotifyItems.FunctionListener_DropDownItems);
            }
        }

        private string text_DropDownButton;
        public string Text_DropDownButton
        {
            get { return text_DropDownButton; }
            set
            {
                if (text_DropDownButton == value) return;

                text_DropDownButton = value;

                RaisePropertyChanged(NotifyItems.FunctionListener_Text_DropDownButton);
            }
        }

        private DropDownItem selectedDropDownItem;
        public DropDownItem SelectedDropDownItem
        {
            get { return selectedDropDownItem; }
            set
            {
                if (selectedDropDownItem == value) return;

                selectedDropDownItem = value;

                RaisePropertyChanged(NotifyItems.FunctionListener_SelectedDropDownItem);
            }
        }

        private List<clsOntologyItem> appliedItems = new List<clsOntologyItem>();
        public List<clsOntologyItem> AppliedItems
        {
            get { return appliedItems; }
            set
            {
                if (appliedItems == value) return;

                appliedItems = value;

                RaisePropertyChanged(NotifyItems.FunctionListener_AppliedItems);
            }
        }

        private bool isChecked_Activate;
        public bool IsChecked_Activate
        {
            get { return isChecked_Activate; }
            set
            {
                if (isChecked_Activate == value) return;

                isChecked_Activate = value;

                RaisePropertyChanged(NotifyItems.FunctionListener_IsChecked_Activate);
            }
        }

        private string text_Activate;
        public string Text_Activate
        {
            get { return text_Activate; }
            set
            {
                if (text_Activate == value) return;

                text_Activate = value;

                RaisePropertyChanged(NotifyItems.FunctionListener_Text_Activate);
            }
        }

        private string text_OpenDoc;
        public string Text_OpenDoc
        {
            get { return text_OpenDoc; }
            set
            {
                if (text_OpenDoc == value) return;

                text_OpenDoc = value;

                RaisePropertyChanged(NotifyItems.FunctionListener_Text_OpenDoc);
            }
        }

        private bool isEnabled_OpenDoc;
        public bool IsEnabled_OpenDoc
        {
            get { return isEnabled_OpenDoc; }
            set
            {
                if (isEnabled_OpenDoc == value) return;

                isEnabled_OpenDoc = value;

                RaisePropertyChanged(NotifyItems.FunctionListener_IsEnabled_OpenDoc);
            }
        }

        private bool isVisible_OpenDoc;
        public bool IsVisible_OpenDoc
        {
            get { return isVisible_OpenDoc; }
            set
            {
                if (isVisible_OpenDoc == value) return;

                isVisible_OpenDoc = value;

                RaisePropertyChanged(NotifyItems.FunctionListener_IsVisible_OpenDoc);
            }
        }

        private bool isVisible_OpenDocText;
        public bool IsVisible_OpenDocText
        {
            get { return isVisible_OpenDocText; }
            set
            {
                if (isVisible_OpenDocText == value) return;

                isVisible_OpenDocText = value;

                RaisePropertyChanged(NotifyItems.FunctionListener_IsVisible_OpenDocText);
            }
        }

        private Image image_OpenDoc;
        public Image Image_OpenDoc
        {
            get { return image_OpenDoc; }
            set
            {
                if (image_OpenDoc == value) return;

                image_OpenDoc = value;

                RaisePropertyChanged(NotifyItems.FunctionListener_Image_OpenDoc);
            }
        }



        private string text_InsertBookmark;
        public string Text_InsertBookmark
        {
            get { return text_InsertBookmark; }
            set
            {
                if (text_InsertBookmark == value) return;

                text_InsertBookmark = value;

                RaisePropertyChanged(NotifyItems.FunctionListener_Text_InsertBookmark);
            }
        }

        private bool isEnabled_InsertBookmark;
        public bool IsEnabled_InsertBookmark
        {
            get { return isEnabled_InsertBookmark; }
            set
            {
                if (isEnabled_InsertBookmark == value) return;

                isEnabled_InsertBookmark = value;

                RaisePropertyChanged(NotifyItems.FunctionListener_IsEnabled_InsertBookmark);
            }
        }

        private bool isVisible_InsertBookmark;
        public bool IsVisible_InsertBookmark
        {
            get { return isVisible_InsertBookmark; }
            set
            {
                if (isVisible_InsertBookmark == value) return;

                isVisible_InsertBookmark = value;

                RaisePropertyChanged(NotifyItems.FunctionListener_IsVisible_InsertBookmark);
            }
        }

        private bool isVisible_InsertBookmarkText;
        public bool IsVisible_InsertBookmarkText
        {
            get { return isVisible_InsertBookmarkText; }
            set
            {
                if (isVisible_InsertBookmarkText == value) return;

                isVisible_InsertBookmarkText = value;

                RaisePropertyChanged(NotifyItems.FunctionListener_IsVisible_InsertBookmarkText);
            }
        }

        private Image image_InsertBookmark;
        public Image Image_InsertBookmark
        {
            get { return image_InsertBookmark; }
            set
            {
                if (image_InsertBookmark == value) return;

                image_InsertBookmark = value;

                RaisePropertyChanged(NotifyItems.FunctionListener_Image_InsertBookmark);
            }
        }

        private string text_ActivateBookmark;
        public string Text_ActivateBookmark
        {
            get { return text_ActivateBookmark; }
            set
            {
                if (text_ActivateBookmark == value) return;

                text_ActivateBookmark = value;

                RaisePropertyChanged(NotifyItems.FunctionListener_Text_ActivateBookmark);
            }
        }

        private bool isEnabled_ActivateBookmark;
        public bool IsEnabled_ActivateBookmark
        {
            get { return isEnabled_ActivateBookmark; }
            set
            {
                if (isEnabled_ActivateBookmark == value) return;

                isEnabled_ActivateBookmark = value;

                RaisePropertyChanged(NotifyItems.FunctionListener_IsEnabled_ActivateBookmark);
            }
        }

        private bool isVisible_ActivateBookmark;
        public bool IsVisible_ActivateBookmark
        {
            get { return isVisible_ActivateBookmark; }
            set
            {
                if (isVisible_ActivateBookmark == value) return;

                isVisible_ActivateBookmark = value;

                RaisePropertyChanged(NotifyItems.FunctionListener_IsVisible_ActivateBookmark);
            }
        }

        private bool isVisible_ActivateBookmarkText;
        public bool IsVisible_ActivateBookmarkText
        {
            get { return isVisible_ActivateBookmarkText; }
            set
            {
                if (isVisible_ActivateBookmarkText == value) return;

                isVisible_ActivateBookmarkText = value;

                RaisePropertyChanged(NotifyItems.FunctionListener_IsVisible_ActivateBookmarkText);
            }
        }

        private Image image_ActivateBookmark;
        public Image Image_ActivateBookmark
        {
            get { return image_ActivateBookmark; }
            set
            {
                if (image_ActivateBookmark == value) return;

                image_ActivateBookmark = value;

                RaisePropertyChanged(NotifyItems.FunctionListener_Image_ActivateBookmark);
            }
        }

        private string text_InsertItems;
        public string Text_InsertItems
        {
            get { return text_InsertItems; }
            set
            {
                if (text_InsertItems == value) return;

                text_InsertItems = value;

                RaisePropertyChanged(NotifyItems.FunctionListener_Text_InsertItems);
            }
        }

        private bool isEnabled_InsertItems;
        public bool IsEnabled_InsertItems
        {
            get { return isEnabled_InsertItems; }
            set
            {
                if (isEnabled_InsertItems == value) return;

                isEnabled_InsertItems = value;

                RaisePropertyChanged(NotifyItems.FunctionListener_IsEnabled_InsertItems);
            }
        }

        private bool isVisible_InsertItems;
        public bool IsVisible_InsertItems
        {
            get { return isVisible_InsertItems; }
            set
            {
                if (isVisible_InsertItems == value) return;

                isVisible_InsertItems = value;

                RaisePropertyChanged(NotifyItems.FunctionListener_IsVisible_InsertItems);
            }
        }

        private bool isVisible_InsertItemsText;
        public bool IsVisible_InsertItemsText
        {
            get { return isVisible_InsertItemsText; }
            set
            {
                if (isVisible_InsertItemsText == value) return;

                isVisible_InsertItemsText = value;

                RaisePropertyChanged(NotifyItems.FunctionListener_IsVisible_InsertItemsText);
            }
        }

        private Image image_InsertItems;
        public Image Image_InsertItems
        {
            get { return image_InsertItems; }
            set
            {
                if (image_InsertItems == value) return;

                image_InsertItems = value;

                RaisePropertyChanged(NotifyItems.FunctionListener_Image_InsertItems);
            }
        }

        private string text_Form;
        public string Text_Form
        {
            get { return text_Form; }
            set
            {
                if (text_Form == value) return;

                text_Form = value;

                RaisePropertyChanged(NotifyItems.FunctionListener_Text_Form);
            }
        }

        private List<clsOntologyItem> selectedItems;
        public List<clsOntologyItem> SelectedItems
        {
            get { return selectedItems; }
            set
            {

                selectedItems = value;

                RaisePropertyChanged(NotifyItems.FunctionListener_SelectedItems);

            }
        }

        private FormMessage formMessage;
        public FormMessage FormMessage
        {
            get { return formMessage; }
            set
            {

                formMessage = value;

                RaisePropertyChanged(NotifyItems.FunctionListener_FormMessage);
            }
        }

        private string text_AddItem;
        public string Text_AddItem
        {
            get { return text_AddItem; }
            set
            {
                if (text_AddItem == value) return;

                text_AddItem = value;

                RaisePropertyChanged(NotifyItems.FunctionListener_Text_AddItem);
            }
        }


        private bool isEnabled_AddItem;
        public bool IsEnabled_AddItem
        {
            get { return isEnabled_AddItem; }
            set
            {
                if (isEnabled_AddItem == value) return;

                isEnabled_AddItem = value;

                RaisePropertyChanged(NotifyItems.FunctionListener_IsEnabled_AddItem);

            }
        }

        private bool isVisible_AddItem;
        public bool IsVisible_AddItem
        {
            get { return isVisible_AddItem; }
            set
            {
                if (isVisible_AddItem == value) return;

                isVisible_AddItem = value;

                RaisePropertyChanged(NotifyItems.FunctionListener_IsVisible_AddItem);

            }
        }

        private string text_SelectedButton;
        public string Text_SelectedButton
        {
            get { return text_SelectedButton; }
            set
            {
                if (text_SelectedButton == value) return;

                text_SelectedButton = value;

                RaisePropertyChanged(NotifyItems.FunctionListener_Text_SelectedButton);
            }
        }
    }
}
