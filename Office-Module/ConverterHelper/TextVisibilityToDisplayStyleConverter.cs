﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Office_Module.ConverterHelper
{
    public static class TextVisibilityToDisplayStyleConverter
    {
        public static ToolStripItemDisplayStyle Convert(bool isVisible)
        {
            return isVisible ? ToolStripItemDisplayStyle.ImageAndText : ToolStripItemDisplayStyle.Image;
        }
    }
}
