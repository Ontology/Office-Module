﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Office_Module.ConverterHelper
{
    public delegate void ClickedDropDownButton(DropDownItem dropDownItem);
    public class DropDownItem
    {
        public string DisplayName { get; set; }
        public ToolStripItem ToolStripDropDownItem { get; private set; }
        public ToolStripDropDownButton DropDownButton { get; private set; }
        public event ClickedDropDownButton clickedDropDownButton;
        public int OrderId { get; set; }
        public bool IsDefault { get; set; }

        public void AssignDropDownButton(ToolStripDropDownButton dropDownButton)
        {
            DropDownButton = dropDownButton;

            ToolStripDropDownItem = DropDownButton.DropDownItems.Add(DisplayName, null, new EventHandler(clickDropDownButton));
        }

        private void clickDropDownButton(object sender, EventArgs e)
        {
            if (clickedDropDownButton != null)
            {
                clickedDropDownButton(this);
            }
        }

        public DropDownItem(string displayName)
        {
            DisplayName = displayName;
        }
    }
}
