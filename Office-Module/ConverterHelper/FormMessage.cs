﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Office_Module.ConverterHelper
{
    [Flags]
    public enum FormMessageType
    {
        Unknown = 0,
        OK = 1,
        Exclamation = 2,
        Information = 4
    }
    public class FormMessage
    {
        public string Message { get; set; }
        public string Short { get; set; }
        public FormMessageType FormMessageType { get; set; }

        public FormMessage(string message, string shortMessage, FormMessageType formMessageType)
        {
            Message = message;
            Short = shortMessage;
            FormMessageType = formMessageType;
        }
    }
}
